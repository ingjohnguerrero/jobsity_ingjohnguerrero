<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Twitter;
use Auth;
use App\User;

class Tweet extends Model
{
    //
    public static function getAll($twitter_username){
    	$user = User::where('twitter_username', '=', $twitter_username)->first();
    	$localTweets = Tweet::where('user_id', '=', $user->id)->lists('twitter_id')->toArray();
    	$serverTweets = json_decode(Twitter::getUserTimeline(['screen_name' => $twitter_username, 'count' => 20, 'format' => 'json']));

    	$serverParse = function($tweet) use($localTweets){
    		$data = [];
    		$data['id'] = $tweet->id;
    		$data['text'] = $tweet->text;
    		$data['created_at'] = $tweet->created_at;
    		$data['hidden'] = (in_array($tweet->id, $localTweets))?true:false;
    		return $data;
    	};

    	$tweetsToShow = array();

    	if($serverTweets){

    		$tweetsToShow = array_map($serverParse, $serverTweets);

    	}

    	return $tweetsToShow;
    }
}

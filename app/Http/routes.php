<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/', 'GuessController@index');
	
	Route::get('/profile/{user_name}', [ 'as' => 'profile', 'uses' => 'ProfilesController@profileById']);

	Route::get('/tweets/{twitter_username}', ['as' => 'user.tweets', 'uses' => 'TweetsController@userTweet']);

	//For loggec in user
    Route::get('/home', 'HomeController@index');

    Route::get('/entry/create', 'EntriesController@create');

    Route::post('/entry/create', 'EntriesController@store');

    Route::get('/entry/edit/{id}', 'EntriesController@edit');

    Route::post('/entry/edit/{id}', 'EntriesController@update');

    Route::post('/tweets/hide', ['middleware' => 'auth' ,'uses' => 'TweetsController@registerTweetToHide']);
});
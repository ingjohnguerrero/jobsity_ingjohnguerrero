<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

//include other methods
use Auth;
use App\Entry;

class EntriesController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show the application entries dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

    }

    /**
     * Show the application entries dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $entry = new Entry();
    	return view('entries.form', ['entry' => $entry]);
    }

    /**
     * Show the application entries dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        //return view('home');
        return Entry::get()->orderBy('created_at', 'desc');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	try {
    		$entry = Entry::findOrFail($id);	
    	} catch (\Exception $e) {
    		//return $e->getMessage();
    		return response('Entry not found.', 404);
    	}

        return view('entries.form', ['entry' => $entry]);
    }

    public function update($id){
        $user = Auth::user();
        $username = $user->user_name;

        try {
            $data = request()->all();
            $entry = Entry::findOrFail($id);
            $entry->title = $data['title'];
            $entry->content = $data['content'];  
            $entry->save();
        } catch (\Exception $e) {
            //return $e->getMessage();
            return response('Entry not found.', 404);
        }
        return redirect()->route('profile', ['user_name' => $username]);
    }

    public function store(){
        $user = Auth::user();
        $username = $user->user_name;

        $data = request()->all();
        unset($data['_token']);

        $data['user_id'] = $user->id;

        $entry = Entry::create($data);

        return redirect()->route('profile', ['user_name' => $username]);
    }
}

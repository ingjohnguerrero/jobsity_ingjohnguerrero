<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

//Imported Models
use App\Tweet;
use App\User;
use App\Entry;
use Auth;


class ProfilesController extends Controller
{
    //User profile
    public function profileById($user_name){
    	$user = User::where('user_name', '=', $user_name)->first();//User::find($id);
    	$twitter_username = $user->twitter_username;

    	if(Auth::guest()){
    		$twitterOwner = false;
    	}else{
    		$twitterOwner = ($twitter_username == Auth::user()->twitter_username)?true:false;
    	}

    	$user_name = $user->user_name;
    	$entries = Entry::where('user_id', '=', $user->id)->get();
    	$tweets = Tweet::getAll($twitter_username);
    	//dd($user);
    	return view('profile.index', ['entries' => $entries, 'twitter_username' => $twitter_username, 'tweets' => $tweets ,'user_name' => $user_name]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

//Included Modules
use App\Tweet;

class TweetsController extends Controller
{
    public function userTweet($twitter_username){
    	$tweets = Tweet::getAll($twitter_username);
    	return $tweets;//response()->json($tweets);
    }

    public function registerTweetToHide(){

    	$error = 0;

    	$msg = "Success";

    	$responseData = null;

    	$user = Auth::user();

    	$data = request()->all();

    	$tweet = Tweet::where('twitter_id', '=', $data['twitter_id'])->first();

    	try {
    		if($tweet){
    			$responseData = $tweet;
    			$tweet->delete();
    		}else{
    			$tweet = Tweet();
    			$tweet->twitter_id = $data['twitter_id'];
    			$tweet->user_id = $user->id;
    			$save();
    			$responseData = $tweet;
    		}
    	} catch (\Exception $e) {
    		$error = 1;

    		$msg = "Error: ". $e->getMessage();

    		$responseData = null;

    	}

    	return ['error' => $error, 'msg' => $msg, 'data' => $responseData];

    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

//Included modules
use App\Entry;

class GuessController extends Controller
{
    public function index(){
    	$entries = Entry::orderBy('created_at','desc')->get();
    	//dd($entries);
    	return view('welcome', ['entries' => $entries]);
    }
}

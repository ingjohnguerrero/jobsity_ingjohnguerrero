<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Entry;

use App\Tweet;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::guest()){
            $entries = Entry::with('user')->orderBy('created_at','desc')->get();
            return view('home', ['entries' => $entries]);
        }else{
            $user = Auth::user();
            $username = $user->user_name;
            $tweets = Tweet::getAll($user->twitter_username);
            $entries = Entry::with('user')->where('user_id', '=', $user->id)->orderBy('created_at','desc')->get();
            return view('home', ['username' => $username, 'entries' => $entries, 'tweets' => $tweets]);
        }
    }

}

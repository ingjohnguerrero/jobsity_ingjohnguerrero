@extends('layouts.app')

@section('scripts')
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-7 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Entries of {{$user_name}} </div>

                <div class="panel-body">
                    @each('entries.entry', $entries, 'entry', 'entries.empty')
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">tweets of {{$twitter_username}} </div>

                <div class="panel-body">
                    @if(Auth::guest())
                        @each('tweet.priventry', $tweets, 'tweet', 'tweet.empty')
                    @else
                        @if(Auth::user()->twitter_username == $twitter_username)
                            @each('tweet.entry', $tweets, 'tweet', 'tweet.empty')
                        @else
                            @each('tweet.priventry', $tweets, 'tweet', 'tweet.empty')
                        @endif
                        
                    @endif
                </div>
            </div>
        </div>
        
    </div>
</div>
@endsection

@extends('layouts.app')

@section('scripts')
<script src="{{ asset('js/libs/angular.min.js') }}"></script>
<script src="{{ asset('js/profile/core.js') }}"></script>
<script src="{{ asset('js/profile/controllers.js') }}"></script>
<script src="{{ asset('js/profile/services.js') }}"></script>
<script>
    window.tweetInfo = "{{ url('/tweets/'.$twitter_username) }}";
</script>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Entries of {{$user_name}} </div>

                <div class="panel-body">
                    @each('entries.entry', $entries, 'entry', 'entries.empty')
                </div>
            </div>
        </div>
        <div ng-app="jobsityProfile" class="col-md-2">
            <div ng-controller="jobsityProfile">
            
            </div>
        </div>
        
    </div>
</div>
@endsection

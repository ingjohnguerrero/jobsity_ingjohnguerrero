<div class="col-md-8">
	<h1>{{{$entry->title}}} </h1>
	<h4>{{{$entry->created_at}}} </h4>
	<p>{{{$entry->content}}} </p>
	<a class="btn btn-primary" href="{{ url('/profile', $entry->user->user_name) }}" >{{$entry->user->user_name}}</a>
</div>
@if (!Auth::guest())
	@if(Auth::user()->id == $entry->user_id)
		<div class="col-md-2">
			<a class="btn btn-primary" href="{{ url('/entry/edit', $entry->id) }}" >Edit</a>
		</div>
	@endif
@endif
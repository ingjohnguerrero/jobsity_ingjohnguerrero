@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-7 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Entries</div>

                <div class="panel-body">
                    @each('entries.entry', $entries, 'entry', 'entries.empty')
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">tweets</div>

                <div class="panel-body">
                    @each('tweet.entry', $tweets, 'tweet', 'tweet.empty')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

/**
* profile.Controllers Module
*
* Controllers for profile view
*/
angular.module('profile.Controllers', [])
.controller('profileCtrl', ['$scope', 'Tweets', function($scope, Tweets){
	$scope.tweets = Tweets.all();
	
}])
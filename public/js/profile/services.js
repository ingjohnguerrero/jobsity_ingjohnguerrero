/**
* profile.services Module
*
* Service dedicated to handle twitter info
*/
angular.module('profile.services', []).
factory('Tweets', [function(){

	var tweets = [];

	return {
		all: function () {
			$http.get(window.tweetInfo)
    		.then(function(response) {
        		tweets = response.data;
        		return tweets;
    		});
		}
	};
}])
/**
* jobsityProfile Module
*
* Core angular file
*/
angular.module('jobsityProfile', ['profile.controllers', 'profile.services'])
.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
})
.run([function(){
	
}])